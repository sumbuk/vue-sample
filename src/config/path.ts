export const path = {
    base: (port: number = 4000) => `http://localhost:${port}/`,
    countries: () => 'countries',
    states: () => 'states',
    cities: () => 'cities',
    users: () => 'users',
};
